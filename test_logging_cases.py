#!/usr/bin/env python2

""" Followed CIS_Amazon_Web_Services_Foundations_Benchmark_v1.2.0 """

import unittest
import lucid
import subprocess
import json
import base64
import time

#import pdb; pdb.set_trace()
# --profile iac


class CloudTrailTest(unittest.TestCase):

    def setUp(self):
        cmd = "aws cloudtrail describe-trails --output=json"
        try:
            trailsList = subprocess.check_output(cmd, shell=True)
            self.trailsList = json.loads(trailsList.decode('utf-8'))
        except:
            self.fail("Unable to get cloudtrail list, please run it again")


# 2.1 Ensure CloudTrail is enabled in all regions
class CloudTrailEnabledTest(CloudTrailTest):

    def runTest(self):
        section = "2.1"
        sectionstatement = "Ensure CloudTrail is enabled in all regions"
        if len(self.trailsList['trailList']) > 0:

            for trail_obj in self.trailsList['trailList']:
                trailName = trail_obj['Name']
                assertEqualHandler(self, trail_obj['IsMultiRegionTrail'], True, section,
                                   sectionstatement, trailName + " Cloudtrail enabled for all regions")
                try:
                    loggingStatus = subprocess.check_output("aws cloudtrail get-trail-status --name " + trailName, shell=True)
                    loggingStatus = json.loads(loggingStatus.decode('utf-8'))
                except:
                    asertFailHandler(self, section, sectionstatement, "Unable to get trail status for " + trailName + ", please run it again")
                assertEqualHandler(self, loggingStatus['IsLogging'], True, section, sectionstatement, trailName + " Logging is on")
                try:
                    eventStatus = subprocess.check_output("aws cloudtrail get-event-selectors --trail-name " + trailName, shell=True)
                    eventStatus = json.loads(eventStatus.decode('utf-8'))
                except:
                    asertFailHandler(self, section, sectionstatement, "Unable to get event selectors for " + trailName + ", please run it again")
                if len(eventStatus['EventSelectors']) > 0:
                    for eventCounter in range(len(eventStatus['EventSelectors'])):
                        assertEqualHandler(self, eventStatus['EventSelectors'][eventCounter]['IncludeManagementEvents'],
                                           True, section, sectionstatement, trailName + " Event Selector is there")
                        assertEqualHandler(self, eventStatus['EventSelectors'][eventCounter]['ReadWriteType'],
                                           'All', section, sectionstatement, trailName + " Event Selector is there")
                else:
                    asertFailHandler(self, section, sectionstatement, 'No Event Selector set for trail ' + trailName)

        else:
            asertFailHandler(self, section, sectionstatement, 'CloudTrail is not enabled')


# 2.2 Ensure CloudTrail log file validation is enabled
class CloudTrailLogFileValidTest(CloudTrailTest):

    def runTest(self):
        section = "2.2"
        sectionstatement = "Ensure CloudTrail log file validation is enabled"
        if len(self.trailsList['trailList']) > 0:
            for trail_obj in self.trailsList['trailList']:
                assertEqualHandler(self, trail_obj['LogFileValidationEnabled'], True, section, sectionstatement,
                                   trail_obj['Name'] + " Cloudtrail log file validation should be enabled")
        else:
            asertFailHandler(self, section, sectionstatement, 'CloudTrail is not enabled')

# 2.3 Ensure the S3 bucket used to store CloudTrail logs is not publicly accessible


class S3BucketTrailAccessTest(CloudTrailTest):

    def runTest(self):
        section = "2.3"
        sectionstatement = "Ensure the S3 bucket used to store CloudTrail logs is not publicly accessible"
        if len(self.trailsList['trailList']) > 0:
            for trail_obj in self.trailsList['trailList']:
                bucketName = trail_obj['S3BucketName']
                try:
                    aclList = subprocess.check_output("aws s3api get-bucket-acl --bucket " + bucketName, shell=True)
                    aclList = json.loads(aclList.decode('utf-8'))
                except:
                    asertFailHandler(self, section, sectionstatement, "Unable to get bucket acl for bucket " + bucketName + ", please run it again")
                uri = [acl['Grantee']['URI'] for acl in aclList['Grants'] if 'URI' in acl['Grantee']]
                for each_uri in uri:
                    assertNotEqualHandler(self, each_uri, 'http://acs.amazonaws.com/groups/global/AllUsers', section, sectionstatement,
                                          bucketName + " bucket should not be publicly accessible")
                    assertNotEqualHandler(self, each_uri, 'http://acs.amazonaws.com/groups/global/AuthenticatedUsers', section, sectionstatement,
                                          bucketName + " bucket access should not be given to Authenticated users")
                    try:
                        bucketPolicy = subprocess.check_output("aws s3api get-bucket-policy --bucket " + bucketName, shell=True)
                        bucketPolicy = json.loads(bucketPolicy.decode('utf-8'))
                        bucketPolicy = json.loads(bucketPolicy['Policy'])
                        bucketPrincipal = [policy['Principal'] for policy in bucketPolicy['Statement'] if policy['Effect'] == 'Allow']
                    except:
                        asertFailHandler(self, section, sectionstatement, "Unable to get bucket policy for bucket " +
                                         bucketName + ", please run it again")
                    for everyPrincipal in bucketPrincipal:
                        assertNotEqualHandler(self, everyPrincipal, '*', section, sectionstatement,
                                              bucketName + " bucket have public access,Need to remove it")
                        assertNotEqualHandler(self, everyPrincipal, '{"AWS" : "*"}', section,
                                              sectionstatement, bucketName + " bucket have public access,Need to remove it")
        else:
            asertFailHandler(self, section, sectionstatement, 'CloudTrail is not enabled')

# 2.4 Ensure CloudTrail trails are integrated with CloudWatch Logs


class CloudWatchLogsEnabledTest(CloudTrailTest):

    def runTest(self):
        section = "2.4"
        sectionstatement = "Ensure CloudTrail trails are integrated with CloudWatch Logs"
        if len(self.trailsList['trailList']) > 0:
            for trail_obj in self.trailsList['trailList']:
                if 'CloudWatchLogsLogGroupArn' in trail_obj:
                    assertIsNotNoneHandler(self, trail_obj['CloudWatchLogsLogGroupArn'], section, sectionstatement, "CloudWatch Logs not enabled")
                    try:
                        loggingStatus = subprocess.check_output("aws cloudtrail get-trail-status --name " + trail_obj['Name'], shell=True)
                        loggingStatus = json.loads(loggingStatus.decode('utf-8'))
                        currentTime = time.time()
                        setRecentTime = False
                        if loggingStatus['LatestCloudWatchLogsDeliveryTime'] <= currentTime and loggingStatus['LatestCloudWatchLogsDeliveryTime'] >= currentTime - 86400:
                            setRecentTime = True
                    except:
                        asertFailHandler(self, section, sectionstatement, "Unable to get trail status for " +
                                         trail_obj['Name'] + ", please run it again")

                    assertEqualHandler(self, setRecentTime, True, section, sectionstatement,
                                       "Cloudtrails are not getting delivered to cloudWatch logs")
                else:
                    asertFailHandler(self, section, sectionstatement, 'CloudWatch Logs not enabled')

        else:
            asertFailHandler(self, section, sectionstatement, 'CloudTrail is not enabled')

# 2.5 Ensure AWS Config is enabled in all regions


class AWSConfigEnabledTest(unittest.TestCase):

    def setUp(self):
        cmd = "aws configservice describe-configuration-recorders --output=json"
        try:
            awsConfigRecordersList = subprocess.check_output(cmd, shell=True)
            self.awsConfigRecordersList = json.loads(awsConfigRecordersList.decode('utf-8'))
        except:
            self.fail("Unable to run the test case, please run it again")

    def runTest(self):
        section = "2.5"
        sectionstatement = "Ensure AWS Config is enabled in all regions"
        recorderNameArray = []
        for configRecorder in self.awsConfigRecordersList['ConfigurationRecorders']:
            try:
                if configRecorder['recordingGroup']['allSupported'] == 'true' and configRecorder['recordingGroup']['includeGlobalResourceTypes'] == 'true':
                    recorderNameArray.append(configRecorder['name'])
                recorderStatus = subprocess.check_output("aws configservice describe-configuration-recorder-status", shell=True)
                recorderStatus = json.loads(recorderStatus.decode('utf-8'))
            except:
                asertFailHandler(self, section, sectionstatement, "Unable to configuration recorder status, please run it again")

            for status in recorderStatus['ConfigurationRecordersStatus']:
                if status['name'] in recorderNameArray and status['recording'] == 'true':
                    assertEqualHandler(self, status['lastStatus'], 'SUCCESS', section, sectionstatement,
                                       status['name'] + 'recorder should be enabled in all regions')
        if len(self.awsConfigRecordersList['ConfigurationRecorders']) == 0:
            asertFailHandler(self, section, sectionstatement, 'No AWS Config recorder is enabled')

# 2.6 Ensure S3 bucket access logging is enabled on the CloudTrail S3 bucket


class BucketAccessLoggingEnabledTest(CloudTrailTest):

    def runTest(self):
        section = "2.6"
        sectionstatement = "Ensure S3 bucket access logging is enabled on the CloudTrail S3 bucket"
        if len(self.trailsList['trailList']) > 0:
            for trail in self.trailsList['trailList']:
                try:
                    bucketLoggingEnabled = subprocess.check_output("aws s3api get-bucket-logging --bucket " + trail['S3BucketName'], shell=True)
                    if bucketLoggingEnabled:
                        bucketLoggingEnabled = json.loads(bucketLoggingEnabled.decode('utf-8'))
                except:
                    asertFailHandler(self, section, sectionstatement, "Unable to get bucket logging for bucket " +
                                     trail['S3BucketName'] + ", please run it again")
                assertNotEqualHandler(self, bucketLoggingEnabled, b'', section, sectionstatement, 'For S3 bucket ' +
                                      trail['S3BucketName'] + 'access logging should be enabled')
        else:
            asertFailHandler(self, section, sectionstatement,
                             'CloudTrail should be enabled before checking the activation of S3 bucket access looging ')


# 2.7 Ensure CloudTrail logs are encrypted at rest using KMS CMKs
class TrailLogsEncryptionTest(CloudTrailTest):

    def runTest(self):
        section = "2.7"
        sectionstatement = "Ensure CloudTrail logs are encrypted at rest using KMS CMKs"
        if len(self.trailsList['trailList']) > 0:
            for trail in self.trailsList['trailList']:
                if 'KmsKeyId' not in trail:
                    asertFailHandler(self, section, sectionstatement, "Cloudtrail logs should be encrypted using KMS CMKs")
        else:
            asertFailHandler(self, section, sectionstatement, 'CloudTrail is not enabled, Please enable it as per security point of view')


# 2.8 Ensure rotation for customer created CMKs is enabled
class CMKKeyRotationEnabledTest(unittest.TestCase):

    def setUp(self):
        cmd = "aws kms list-keys --output=json"
        try:
            keysList = subprocess.check_output(cmd, shell=True)
            self.keysList = json.loads(keysList.decode('utf-8'))
        except:
            self.fail("Unable to get the list of CMK keys, please run it again")

    def runTest(self):
        section = "2.8"
        sectionstatement = "Ensure rotation for customer created CMKs is enabled"
        for key in self.keysList['Keys']:
            try:
                keyRotationStatus = subprocess.check_output("aws kms get-key-rotation-status --key-id " + key['KeyId'], shell=True)
                keyRotationStatus = json.loads(keyRotationStatus.decode('utf-8'))
            except:
                asertFailHandler(self, section, sectionstatement, "Unable to get key rotation status for key " +
                                 key['KeyId'] + ", please run it again")
            assertEqualHandler(self, keyRotationStatus['KeyRotationEnabled'], True, section, sectionstatement,
                               'Please enable the rotation of customer created CMKs option to ensure security ')


def assertEqualHandler(self, arg1, arg2, section, sectionstatement, comment):
    self.assertEqual(arg1, arg2, formatMessage(section, sectionstatement, comment))


def assertNotEqualHandler(self, arg1, arg2, section, sectionstatement, comment):
    self.assertNotEqual(arg1, arg2, formatMessage(section, sectionstatement, comment))


def assertIsNotNoneHandler(self, arg1, section, sectionstatement, comment):
    self.assertIsNotNone(arg1, formatMessage(section, sectionstatement, comment))


def asertFailHandler(self, section, sectionstatement, comment):
    self.fail(formatMessage(section, sectionstatement, comment))


def formatMessage(section, sectionstatement, comment):
    msg = "IASTFERR:%s:%s:%s" % (section, sectionstatement, comment)
    return msg
