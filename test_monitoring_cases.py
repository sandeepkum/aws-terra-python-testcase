#!/usr/bin/env python2

""" Followed CIS_Amazon_Web_Services_Foundations_Benchmark_v1.2.0 """

import unittest
import lucid
import subprocess
import json
import base64
import time

#import pdb; pdb.set_trace()
# --profile iac


class CloudWatchTest(unittest.TestCase):

    def setUp(self):
        cmd = "aws cloudtrail describe-trails --output=json"
        try:
            trailsList = subprocess.check_output(cmd, shell=True)
            self.trailsList = json.loads(trailsList.decode('utf-8'))
        except:
            self.fail("Unable to get cloudtrails list, please run it again")


# 3.1 Ensure a log metric filter and alarm exist for unauthorized API calls
class MetricFilterAlarmEnableUnAuthoriZedCallTest(CloudWatchTest):

    def runTest(self):
        section = "3.1"
        sectionstatement = "Ensure a log metric filter and alarm exist for unauthorized API calls"
        metricFilterAlarmEnabledTest(self, 1, "for Unauthorized/Access Denied api calls", section, sectionstatement)

# 3.2 Ensure a log metric filter and alarm exist for Management Console sign-in without MFA


class MetricFilterAlarmEnableMFAConsoleTest(CloudWatchTest):

    def runTest(self):
        section = "3.2"
        sectionstatement = "Ensure a log metric filter and alarm exist for Management Console sign-in without MFA"
        metricFilterAlarmEnabledTest(self, 2, "for Management Console sign-in without MFA", section, sectionstatement)

# 3.3 Ensure a log metric filter and alarm exist for usage of "root" account


class MetricFilterAlarmRootAccountUsageTest(CloudWatchTest):

    def runTest(self):
        section = "3.3"
        sectionstatement = "Ensure a log metric filter and alarm exist for usage of root account"
        metricFilterAlarmEnabledTest(self, 3, "for usage of Root account", section, sectionstatement)

# 3.4 Ensure a log metric filter and alarm exist for IAM policy changes


class MetricFilterAlarmIAMPolicyTest(CloudWatchTest):

    def runTest(self):
        section = "3.4"
        sectionstatement = "Ensure a log metric filter and alarm exist for IAM policy changes"
        metricFilterAlarmEnabledTest(self, 4, "for changes in IAM policies", section, sectionstatement)

# 3.5 Ensure a log metric filter and alarm exist for CloudTrail configuration changes


class MetricFilterAlarmCloudTrailConfigTest(CloudWatchTest):

    def runTest(self):
        section = "3.5"
        sectionstatement = "Ensure a log metric filter and alarm exist for CloudTrail configuration changes"
        metricFilterAlarmEnabledTest(self, 5, "for changes in CloudTrail configurations", section, sectionstatement)

# 3.6 Ensure a log metric filter and alarm exist for AWS Management Console authentication failures


class MetricFilterAWSManagementConsoleAuthFailTest(CloudWatchTest):

    def runTest(self):
        section = "3.6"
        sectionstatement = "Ensure a log metric filter and alarm exist for AWS Management Console authentication failures"
        metricFilterAlarmEnabledTest(self, 6, "for AWS Management Console authentication failure", section, sectionstatement)

# 3.7 Ensure a log metric filter and alarm exist for disabling or scheduled deletion of customer created CMKs


class MetricFilterCustomerCMKTest(CloudWatchTest):

    def runTest(self):
        section = "3.7"
        sectionstatement = "Ensure a log metric filter and alarm exist for disabling or scheduled deletion of customer created CMKs"
        metricFilterAlarmEnabledTest(self, 7, "for disabling or scheduled deletion of customer created CMKs", section, sectionstatement)

# 3.8 Ensure a log metric filter and alarm exist for S3 bucket policy changes


class MetricFilterS3BucketPolicyTest(CloudWatchTest):

    def runTest(self):
        section = "3.8"
        sectionstatement = "Ensure a log metric filter and alarm exist for S3 bucket policy changes"
        metricFilterAlarmEnabledTest(self, 8, "for changes in S3 bucket policies", section, sectionstatement)

# 3.9 Ensure a log metric filter and alarm exist for AWS Config configuration changes


class MetricFilterAWSConfigChange(CloudWatchTest):

    def runTest(self):
        section = "3.9"
        sectionstatement = "Ensure a log metric filter and alarm exist for AWS Config configuration changes"
        metricFilterAlarmEnabledTest(self, 9, "for chnages in AWS Config configurations", section, sectionstatement)

# 3.10 Ensure a log metric filter and alarm exist for security group changes


class MetricFilterSecurityGroupChange(CloudWatchTest):

    def runTest(self):
        section = "3.10"
        sectionstatement = "Ensure a log metric filter and alarm exist for security group changes"
        metricFilterAlarmEnabledTest(self, 10, "for changes in security group", section, sectionstatement)

# 3.11 Ensure a log metric filter and alarm exist for changes to Network Access Control Lists (NACL)


class MetricFilterNACLChanges(CloudWatchTest):

    def runTest(self):
        section = "3.11"
        sectionstatement = "Ensure a log metric filter and alarm exist for changes to Network Access Control Lists (NACL)"
        metricFilterAlarmEnabledTest(self, 11, "for changes in network access control list", section, sectionstatement)

# 3.12 Ensure a log metric filter and alarm exist for changes to network gateways


class MetricFilterNetworkGatewayChanges(CloudWatchTest):

    def runTest(self):
        section = "3.12"
        sectionstatement = "Ensure a log metric filter and alarm exist for changes to network gateways"
        metricFilterAlarmEnabledTest(self, 12, "for changes in network gateways", section, sectionstatement)

# 3.13 Ensure a log metric filter and alarm exist for route table changes


class MetricFilterRouteTableChanges(CloudWatchTest):

    def runTest(self):
        section = "3.13"
        sectionstatement = "Ensure a log metric filter and alarm exist for route table changes"
        metricFilterAlarmEnabledTest(self, 13, "for changes in route table", section, sectionstatement)

# 3.14 Ensure a log metric filter and alarm exist for VPC changes


class MetricFilterVPCChanges(CloudWatchTest):

    def runTest(self):
        section = "3.14"
        sectionstatement = "Ensure a log metric filter and alarm exist for VPC changes"
        metricFilterAlarmEnabledTest(self, 14, "for VPC changes", section, sectionstatement)


def metricFilterAlarmEnabledTest(self, testno, errormsg, section, sectionstatement):
    if len(self.trailsList['trailList']) > 0:
        for trail_obj in self.trailsList['trailList']:
            assertEqualHandler(self, trail_obj['IsMultiRegionTrail'], True, section, sectionstatement,
                               trail_obj['Name'] + " Cloudtrail enabled for all regions")
            if trail_obj['CloudWatchLogsLogGroupArn']:
                CloudtrailLogGroupName = trail_obj['CloudWatchLogsLogGroupArn'].split(":")[6]
            try:
                loggingStatus = subprocess.check_output("aws cloudtrail get-trail-status --name " + trail_obj['Name'], shell=True)
                loggingStatus = json.loads(loggingStatus.decode('utf-8'))
            except:
                assertFailHandler(self, section, sectionstatement, "Unable to get trail status for " + trail_obj['Name'] + ", please run it again")
            assertEqualHandler(self, loggingStatus['IsLogging'], True, section, sectionstatement, trail_obj['Name'] + " Logging is on")
            try:
                eventStatus = subprocess.check_output("aws cloudtrail get-event-selectors --trail-name " + trail_obj['Name'], shell=True)
                eventStatus = json.loads(eventStatus.decode('utf-8'))
            except:
                assertFailHandler(self, section, sectionstatement, "Unable to get event selectors for " + trail_obj['Name'] + ", please run it again")
            if len(eventStatus['EventSelectors']) > 0:
                for eventCounter in range(len(eventStatus['EventSelectors'])):
                    assertEqualHandler(self, eventStatus['EventSelectors'][eventCounter]['IncludeManagementEvents'],
                                       True, section, sectionstatement, trail_obj['Name'] + " Event Selector is there")
                    assertEqualHandler(self, eventStatus['EventSelectors'][eventCounter]['ReadWriteType'],
                                       'All', section, sectionstatement, trail_obj['Name'] + " Event Selector is there")
            else:
                assertFailHandler(self, section, sectionstatement, 'No Event Selector set for trail ' + trail_obj['Name'])
            try:
                metericFiltersList = subprocess.check_output(
                    "aws logs describe-metric-filters --log-group-name " + CloudtrailLogGroupName, shell=True)
                metericFiltersList = json.loads(metericFiltersList.decode('utf-8'))
            except:
                assertFailHandler(self, section, sectionstatement, "Unable to get metric filters for " +
                                  CloudtrailLogGroupName + ", please run it again")
            paricularMetricConfigure = 0
            if len(metericFiltersList['metricFilters']) > 0:
                for metricFilter in metericFiltersList['metricFilters']:
                    filterResult = matchFilterPatterns(self, testno, metricFilter)
                    if filterResult:
                        paricularMetricConfigure += 1
                        try:
                            metricAlarms = subprocess.check_output("aws cloudwatch describe-alarms", shell=True)
                            metricAlarms = json.loads(metricAlarms.decode('utf-8'))
                        except:
                            assertFailHandler(self, section, sectionstatement, "Unable to get metric filter alarms list, please run it again")
                        particularMetriAlarm = 0
                        if len(metricAlarms['MetricAlarms']) > 0:
                            for metricAlarm in metricAlarms['MetricAlarms']:
                                if metricAlarm['MetricName'] == metricFilter['metricTransformations'][0]['metricName']:
                                    particularMetriAlarm += 1
                                    try:
                                        snsTopicList = subprocess.check_output(
                                            "aws sns list-subscriptions-by-topic --topic-arn " + metricAlarm['AlarmActions'][0], shell=True)
                                        snsTopicList = json.loads(snsTopicList.decode('utf-8'))
                                    except:
                                        assertFailHandler(self, section, sectionstatement, "Unable to get the sns topic list for alarm " +
                                                          metricAlarm['AlarmName'] + ", please run it again")
                                    # print(metricAlarm['AlarmActions'][0])
                                    assertEqualHandler(self, snsTopicList['Subscriptions'], 0, section, sectionstatement,
                                                       "No active subscriber to the SNS topic " + metricAlarm['AlarmActions'][0])
                                    # if len(snsTopicList['Subscriptions']) > 0:
                                    #     pass
                            if particularMetriAlarm == 0:
                                assertFailHandler(self, section, sectionstatement, "No metric alarm exist " + errormsg + "metric filter " +
                                                  metricFilter['metricTransformations'][0]['metricName'])

                        else:
                            assertFailHandler(self, section, sectionstatement, 'No cloudwatch alarms setup')

                if paricularMetricConfigure == 0:
                    assertFailHandler(self, section, sectionstatement, "No metric filter exist " + errormsg)
            else:
                assertFailHandler(self, section, sectionstatement, 'No Metric filter setup for trail ' + trail_obj['Name'])

    else:
        self.fail('CloudTrail is not enabled')


def matchFilterPatterns(self, testno, metricFilter):
    if testno == 1:
        return metricForUnauthorizedAccessDenied(self, metricFilter)
    if testno == 2:
        return metricForMFAConsoleLogin(self, metricFilter)
    if testno == 3:
        return metricForRootAccountUsage(self, metricFilter)
    if testno == 4:
        return metricForIAMPolicyChange(self, metricFilter)
    if testno == 5:
        return metricForCloudTrailConfigChange(self, metricFilter)
    if testno == 6:
        return metricForAWSConsoleAuthFailure(self, metricFilter)
    if testno == 7:
        return metricForCustomerCMK(self, metricFilter)
    if testno == 8:
        return metricForS3BucketPolicy(self, metricFilter)
    if testno == 9:
        return metricForAWSConfigChanges(self, metricFilter)
    if testno == 10:
        return metricForSecurityGroupChanges(self, metricFilter)
    if testno == 11:
        return metricForNACLChanges(self, metricFilter)
    if testno == 12:
        return metricForNetworkgatewayChanges(self, metricFilter)
    if testno == 13:
        return metricForRouteTableChanges(self, metricFilter)
    if testno == 14:
        return metricForVPCChanges(self, metricFilter)


def metricForUnauthorizedAccessDenied(self, metricFilter):
    if 'UnauthorizedOperation' in metricFilter['filterPattern'] or 'AccessDenied' in metricFilter['filterPattern']:
        return True
    return False


def metricForMFAConsoleLogin(self, metricFilter):
    if 'ConsoleLogin' in metricFilter['filterPattern'] and ('$.additionalEventData.MFAUsed' in metricFilter['filterPattern'] and 'Yes' in metricFilter['filterPattern']):
        return True
    return False


def metricForRootAccountUsage(self, metricFilter):
    #################################
    if ('$.userIdentity.type' in metricFilter['filterPattern'] and 'Root' in metricFilter['filterPattern']) and '$.userIdentity.invokedBy NOT EXISTS' in metricFilter['filterPattern'] and ('$.eventType' in metricFilter['filterPattern'] and 'AwsServiceEvent' in metricFilter['filterPattern'] and '!=' in metricFilter['filterPattern']):
        return True
    return False


def metricForIAMPolicyChange(self, metricFilter):
    if 'DeleteGroupPolicy' in metricFilter['filterPattern'] or 'DeleteRolePolicy' in metricFilter['filterPattern'] or 'PutRolePolicy' in metricFilter['filterPattern'] or 'PutGroupPolicy' in metricFilter['filterPattern'] or 'DeleteUserPolicy' in metricFilter['filterPattern'] or 'PutUserPolicy' in metricFilter['filterPattern'] or 'CreatePolicy' in metricFilter['filterPattern'] or 'DeletePolicy' in metricFilter['filterPattern'] or 'CreatePolicyVersion' in metricFilter['filterPattern'] or 'DeletePolicyVersion' in metricFilter['filterPattern'] or 'AttachRolePolicy' in metricFilter['filterPattern'] or 'DetachRolePolicy' in metricFilter['filterPattern'] or 'AttachUserPolicy' in metricFilter['filterPattern'] or 'DetachUserPolicy' in metricFilter['filterPattern'] or 'AttachGroupPolicy' in metricFilter['filterPattern'] or 'DetachGroupPolicy' in metricFilter['filterPattern']:
        return True
    return False


def metricForCloudTrailConfigChange(self, metricFilter):
    if 'CreateTrail' in metricFilter['filterPattern'] or 'UpdateTrail' in metricFilter['filterPattern'] or 'ScheduleKeyDeletion' in metricFilter['filterPattern']:
        return True
    return False


def metricForAWSConsoleAuthFailure(self, metricFilter):
    if 'ConsoleLogin' in metricFilter['filterPattern'] or 'Failed authentication' in metricFilter['filterPattern']:
        return True
    return False


def metricForCustomerCMK(self, metricFilter):
    if 'kms.amazonaws.com' in metricFilter['filterPattern'] and ('DisableKey' in metricFilter['filterPattern'] or 'ScheduleKeyDeletion' in metricFilter['filterPattern']):
        return True
    return False


def metricForS3BucketPolicy(self, metricFilter):
    if 's3.amazonaws.com' in metricFilter['filterPattern'] and ('PutBucketAcl' in metricFilter['filterPattern'] or 'PutBucketPolicy' in metricFilter['filterPattern'] or 'PutBucketCors' in metricFilter['filterPattern'] or 'PutBucketLifecycle' in metricFilter['filterPattern'] or 'PutBucketReplication' in metricFilter['filterPattern'] or 'DeleteBucketPolicy' in metricFilter['filterPattern'] or 'DeleteBucketCors' in metricFilter['filterPattern'] or 'DeleteBucketLifecycle' in metricFilter['filterPattern'] or 'DeleteBucketReplication' in metricFilter['filterPattern']):
        return True
    return False


def metricForAWSConfigChanges(self, metricFilter):
    if 'config.amazonaws.com' in metricFilter['filterPattern'] and ('StopConfigurationRecorder' in metricFilter['filterPattern'] or 'DeleteDeliveryChannel' in metricFilter['filterPattern'] or 'PutDeliveryChannel' in metricFilter['filterPattern'] or 'PutConfigurationRecorder' in metricFilter['filterPattern']):
        return True
    return False


def metricForSecurityGroupChanges(self, metricFilter):
    if 'AuthorizeSecurityGroupIngress' in metricFilter['filterPattern'] or 'AuthorizeSecurityGroupEgress' in metricFilter['filterPattern'] or 'RevokeSecurityGroupIngress' in metricFilter['filterPattern'] or 'RevokeSecurityGroupEgress' in metricFilter['filterPattern'] or 'CreateSecurityGroup' in metricFilter['filterPattern'] or 'DeleteSecurityGroup' in metricFilter['filterPattern']:
        return True
    return False


def metricForNACLChanges(self, metricFilter):
    if 'CreateNetworkAcl' in metricFilter['filterPattern'] or 'CreateNetworkAclEntry' in metricFilter['filterPattern'] or 'DeleteNetworkAcl' in metricFilter['filterPattern'] or 'DeleteNetworkAclEntry' in metricFilter['filterPattern'] or 'ReplaceNetworkAclEntry' in metricFilter['filterPattern'] or 'ReplaceNetworkAclAssociation' in metricFilter['filterPattern']:
        return True
    return False


def metricForNetworkgatewayChanges(self, metricFilter):
    if 'CreateCustomerGateway' in metricFilter['filterPattern'] or 'DeleteCustomerGateway' in metricFilter['filterPattern'] or 'AttachInternetGateway' in metricFilter['filterPattern'] or 'CreateInternetGateway' in metricFilter['filterPattern'] or 'DeleteInternetGateway' in metricFilter['filterPattern'] or 'DetachInternetGateway' in metricFilter['filterPattern']:
        return True
    return False


def metricForRouteTableChanges(self, metricFilter):
    if 'CreateRoute' in metricFilter['filterPattern'] or 'CreateRouteTable' in metricFilter['filterPattern'] or 'ReplaceRoute' in metricFilter['filterPattern'] or 'ReplaceRouteTableAssociation' in metricFilter['filterPattern'] or 'DeleteRouteTable' in metricFilter['filterPattern'] or 'DeleteRoute' in metricFilter['filterPattern'] or 'DisassociateRouteTable' in metricFilter['filterPattern']:
        return True
    return False


def metricForVPCChanges(self, metricFilter):
    if 'CreateVpc' in metricFilter['filterPattern'] or 'DeleteVpc' in metricFilter['filterPattern'] or 'ModifyVpcAttribute' in metricFilter['filterPattern'] or 'AcceptVpcPeeringConnection' in metricFilter['filterPattern'] or 'CreateVpcPeeringConnection' in metricFilter['filterPattern'] or 'DeleteVpcPeeringConnection' in metricFilter['filterPattern'] or 'RejectVpcPeeringConnection' in metricFilter['filterPattern'] or 'AttachClassicLinkVpc' in metricFilter['filterPattern'] or 'DetachClassicLinkVpc' in metricFilter['filterPattern'] or 'DisableVpcClassicLink' in metricFilter['filterPattern'] or 'EnableVpcClassicLink' in metricFilter['filterPattern']:
        return True
    return False


def assertEqualHandler(self, arg1, arg2, section, sectionstatement, comment):
    self.assertEqual(arg1, arg2, formatMessage(section, sectionstatement, comment))


def assertNotEqualHandler(self, arg1, arg2, section, sectionstatement, comment):
    self.assertNotEqual(arg1, arg2, formatMessage(section, sectionstatement, comment))


def assertFailHandler(self, section, sectionstatement, comment):
    self.fail(formatMessage(section, sectionstatement, comment))


def formatMessage(section, sectionstatement, comment):
    msg = "IASTFERR:%s:%s:%s" % (section, sectionstatement, comment)
    return msg
