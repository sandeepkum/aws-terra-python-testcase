#!/usr/bin/env python2

""" Followed CIS_Amazon_Web_Services_Foundations_Benchmark_v1.2.0 """


import unittest
import lucid
import subprocess
import json
import base64
import datetime

import enum
# class InfraBaseTestCase(unittest.TestCase):

#     def setUp(self):
#         self.infra = lucid.getInfraInfo('default')  # , 'ap-south-1'


class globalVariablesEnum(enum.Enum):
    passwordLastUsed = 4
    passwordLastChanged = 5
    accesskey1LastUsed = 10
    accesskey2LastUsed = 15
    passwordEnable = 3
    mfaActive = 7
    userName = 0
    accessKey1Active = 8
    accessKey1LastRotated = 9
    accessKey2Active = 13
    accessKey2LastRotated = 14


class RootAccountTest(unittest.TestCase):

    def setUp(self):
        cmd = "aws iam generate-credential-report --output=json"
        cmd2 = "aws iam get-credential-report --output=json"
        try:
            subprocess.check_output(cmd, shell=True)
            cred_report = subprocess.check_output(cmd2, shell=True)
            self.cred_report = json.loads(cred_report.decode('utf-8'))
            self.user_data = base64.b64decode(
                self.cred_report['Content']).decode('utf-8').split("\n")
        except:
            self.fail("Unable to get credential report, please run it again")


# 1.1 Avoid the use of the "root" account
class AvoidRootAccountUsageTest(RootAccountTest):

    def runTest(self):
        user_data = [x for x in self.user_data[1:] if "<root_account>" in x]
        if len(user_data) > 0:
            userDataArray = user_data[0].split(",")
            section = "1.1"
            sectionstatement = "Avoid the use of the root account"
            # Please avoid use of "magic numbers". Should use enumerations #done
            lastUsedRootPwdDate = userDataArray[globalVariablesEnum.passwordLastUsed.value]
            lastUsedRootAccessKey1Date = userDataArray[globalVariablesEnum.accesskey1LastUsed.value]
            lastUsedRootAccessKey2Date = userDataArray[globalVariablesEnum.accesskey2LastUsed.value]
            # Need to check the case when lastUsedRootPwdDate is not "N/A". In that case,
            # we need to see that this is greater than x days old. [x should be defined as a parameter]
            assertEqualHandler(self, lastUsedRootPwdDate, "N/A", section, sectionstatement,
                               "Password of root account was last time used on " + lastUsedRootPwdDate)
            assertEqualHandler(self, lastUsedRootAccessKey1Date, "N/A", section, sectionstatement,
                               "Access key 1 of root account was last time used on " + lastUsedRootAccessKey1Date)
            assertEqualHandler(self, lastUsedRootAccessKey2Date, "N/A", section, sectionstatement,
                               "Access key 2 of root account was used on " + lastUsedRootAccessKey2Date)

# 1.2 Ensure multi-factor authentication (MFA) is enabled for all IAM users that have a console password


class MFAEnabledTest(RootAccountTest):

    def runTest(self):
        section = "1.2"
        sectionstatement = "Ensure multi-factor authentication (MFA) is enabled for all IAM users that have a console password"
        if len(self.user_data) > 0:
            for user in self.user_data[1:]:
                passwordEnabled = user.split(",")[globalVariablesEnum.passwordEnable.value]
                mfaActive = user.split(",")[globalVariablesEnum.mfaActive.value]
                userName = user.split(",")[globalVariablesEnum.userName.value]
                # Need to create a user which has this as false to see what happens
                if passwordEnabled == "true":
                    assertEqualHandler(self, mfaActive, "true", section, sectionstatement,
                                       "MFA is not enabled for " + userName)  # true means enables

# 1.3 Ensure credentials unused for 90 days or greater are disabled


class UnusedCredentialsTest(RootAccountTest):

    def runTest(self):
        section = "1.3"
        sectionstatement = "Ensure credentials unused for 90 days or greater are disabled"
        today_date = datetime.date.today()
        for user in self.user_data[1:]:
            passwordEnabled = user.split(",")[globalVariablesEnum.passwordEnable.value]
            lastUsedRootPwdDate = user.split(",")[globalVariablesEnum.passwordLastUsed.value]
            lastChangedRootPwdDate = user.split(",")[globalVariablesEnum.passwordLastChanged.value]
            userName = user.split(",")[globalVariablesEnum.userName.value]
            accessKey1Active = user.split(",")[globalVariablesEnum.accessKey1Active.value]
            lastUsedRootAccessKey1Date = user.split(",")[globalVariablesEnum.accesskey1LastUsed.value]
            lastRotatedRootAccessKey1Date = user.split(",")[globalVariablesEnum.accessKey1LastRotated.value]
            accessKey2Active = user.split(",")[globalVariablesEnum.accessKey2Active.value]
            lastRotatedRootAccessKey2Date = user.split(",")[globalVariablesEnum.accessKey2LastRotated.value]
            lastUsedRootAccessKey2Date = user.split(",")[globalVariablesEnum.accesskey2LastUsed.value]
            # Needs fixing. Convert the lastUsedRootPwdDate into datetime object and then compare #done
            if passwordEnabled == "true":
                if lastUsedRootPwdDate != "N/A":
                    assertLessEqualHandler(self, calNoOfDays(today_date, lastUsedRootPwdDate), 90, section, sectionstatement, "For " + str(
                        userName) + ", last time pwd was used " + str(calNoOfDays(today_date, lastUsedRootPwdDate)) + " days back ")
                else:
                    assertLessEqualHandler(self, calNoOfDays(today_date, lastChangedRootPwdDate), 90, section, sectionstatement, "For " + str(
                        userName) + ", last time pwd was changed " + str(calNoOfDays(today_date, lastChangedRootPwdDate)) + " days back ")
            # Needs fixing. Convert the lastUsedRootPwdDate into datetime object and then compare #done
            if accessKey1Active == "true":
                if lastUsedRootAccessKey1Date != "N/A":
                    assertLessEqualHandler(self, calNoOfDays(today_date, lastUsedRootAccessKey1Date), 90, section, sectionstatement, "For " + str(
                        userName) + ", last time access_key_1 was used " + str(calNoOfDays(today_date, lastUsedRootAccessKey1Date)) + " days back ")
                else:
                    assertLessEqualHandler(self, calNoOfDays(today_date, lastRotatedRootAccessKey1Date), 90, section, sectionstatement,  "For " + str(
                        userName) + ", last time access_key_1 was rotated " + str(calNoOfDays(today_date, lastRotatedRootAccessKey1Date)) + " days back ")
            # Needs fixing. Convert the lastUsedRootPwdDate into datetime object and then compare #done
            if accessKey2Active == "true":
                if lastUsedRootAccessKey2Date != "N/A":
                    assertLessEqualHandler(self, calNoOfDays(today_date, lastUsedRootAccessKey2Date), 90, section, sectionstatement,  "For " + str(
                        userName) + ", last time access_key_2 was used " + str(calNoOfDays(today_date, lastUsedRootAccessKey2Date)) + " days back ")
                else:
                    assertLessEqualHandler(self, calNoOfDays(today_date, lastRotatedRootAccessKey2Date), 90, section, sectionstatement,  "For " + str(
                        userName) + ", last time access_key_2 was rotated " + str(calNoOfDays(today_date, lastRotatedRootAccessKey2Date)) + " days back ")

# 1.4 Ensure access keys are rotated every 90 days or less (Scored)


class AccessKeysRotate90daysTest(RootAccountTest):

    def runTest(self):
        section = "1.4"
        sectionstatement = "Ensure access keys are rotated every 90 days or less (Scored)"
        today_date = datetime.date.today()
        for user in self.user_data:
            accessKey1Active = user.split(",")[globalVariablesEnum.accessKey1Active.value]
            lastRotatedRootAccessKey1Date = user.split(",")[globalVariablesEnum.accessKey1LastRotated.value]
            accessKey2Active = user.split(",")[globalVariablesEnum.accessKey2Active.value]
            lastRotatedRootAccessKey2Date = user.split(",")[globalVariablesEnum.accessKey2LastRotated.value]
            userName = user.split(",")[globalVariablesEnum.userName.value]
            if accessKey1Active == "true":
                if lastRotatedRootAccessKey1Date != "N/A":
                    assertLessEqualHandler(self, calNoOfDays(today_date, lastRotatedRootAccessKey1Date), 90, section, sectionstatement, "For " + str(
                        userName) + ", last time access_key_1 was rotated " + str(calNoOfDays(today_date, lastRotatedRootAccessKey1Date)) + " days back ")
            if accessKey2Active == "true":
                if lastRotatedRootAccessKey2Date != "N/A":
                    assertLessEqualHandler(self, calNoOfDays(today_date, lastRotatedRootAccessKey2Date), 90, section, sectionstatement, "For " + str(
                        userName) + ", last time access_key_2 was rotated " + str(calNoOfDays(today_date, lastRotatedRootAccessKey2Date)) + " days back ")


class AccountPwdPolicyTest(unittest.TestCase):

    def setUp(self):
        cmd = "aws iam get-account-password-policy --output=json"
        try:
            policy_str = subprocess.check_output(cmd, shell=True)
            self.policy = json.loads(policy_str.decode('utf-8'))
        except:
            self.fail("Unable to get password policy, please run it again")


# 1.5 Ensure IAM password policy requires at least one uppercase letter
# 1.6 Ensure IAM password policy require at least one lowercase letter
# 1.7 Ensure IAM password policy require at least one symbol
# 1.8 Ensure IAM password policy require at least one number
# 1.9 Ensure IAM password policy requires minimum length of 14 or greater
# 1.10 Ensure IAM password policy prevents password reuse
# 1.11 Ensure IAM password policy expires passwords within 90 days or less
class RequireUpperCasePwdTest(AccountPwdPolicyTest):

    def runTest(self):
        assertEqualHandler(self,  self.policy['PasswordPolicy']['RequireUppercaseCharacters'],
                           True, "1.5", "Ensure IAM password policy requires at least one uppercase letter", "Requires at least one uppercase letter password policy is not set")


class RequireLowerCasePwdTest(AccountPwdPolicyTest):

    def runTest(self):
        assertEqualHandler(self,  self.policy['PasswordPolicy']['RequireLowercaseCharacters'],
                           True, "1.6", "Ensure IAM password policy require at least one lowercase letter", "Requires at least one lowercase letter password policy is not set")


class requireSymbolsPwdPolicyTest(AccountPwdPolicyTest):

    def runTest(self):
        assertEqualHandler(self, self.policy['PasswordPolicy']['RequireSymbols'],
                           True, "1.7", "Ensure IAM password policy require at least one symbol", "Requires at least one non-alphanumeric character password policy is not set")


class requireNumbersPwdPolicyTest(AccountPwdPolicyTest):

    def runTest(self):
        assertEqualHandler(self, self.policy['PasswordPolicy']['RequireNumbers'],
                           True, "1.8", "Ensure IAM password policy require at least one number", "Requires at least one number password policy is not set")


class minimumPwdPolicyTest(AccountPwdPolicyTest):

    def runTest(self):

        minlen = 6
        assertEqualHandler(self, self.policy['PasswordPolicy']['MinimumPasswordLength'],
                           minlen, "1.9", "Ensure IAM password policy requires minimum length of 14 or greater", "Requires minimum password length of 14 or greater policy is not set as %d" % minlen)


class passwordReusePolicyTest(AccountPwdPolicyTest):

    def runTest(self):
        # Set this policy from console and then see what is the value of PasswordReusePrevention
        assertEqualHandler(self, 'PasswordReusePrevention' in self.policy['PasswordPolicy'],
                           True, "1.10", "Ensure IAM password policy prevents password reuse", "Password reuse prevention policy not set")


class expirePwdPolicyTest(AccountPwdPolicyTest):

    def runTest(self):
        if self.policy['PasswordPolicy']['ExpirePasswords']:
            # this is not correct. Need to check that this value is less than 90
            assertEqualHandler(self, self.policy['PasswordPolicy']['MaxPasswordAge'],
                               90, "1.11", "Ensure IAM password policy expires passwords within 90 days or less", "MaxPasswordAge should be 90 policy not set")
        else:
            formatMessage("1.11", "Ensure IAM password policy expires passwords within 90 days or less", "MaxPasswordAge should be 90 policy not set")


# 1.12 Ensure no root account access key exists


class RootAccessKeyEnabledTest(RootAccountTest):

    def runTest(self):
        section = "1.12"
        sectionstatement = "Ensure no root account access key exists"
        root_user = [x for x in self.user_data if "<root_account>" in x]
        if len(root_user) > 0:
            accessKey1Active = root_user[0].split(",")[globalVariablesEnum.accessKey1Active.value]
            accessKey2Active = root_user[0].split(",")[globalVariablesEnum.accessKey2Active.value]
            assertEqualHandler(self, accessKey1Active, "false", section, sectionstatement, "Access key 1 of root account disabled")
            assertEqualHandler(self, accessKey2Active, "false", section, sectionstatement, "Access key 2 of root account disabled")


class AccountMfaPolicyTest(unittest.TestCase):

    def setUp(self):
        cmd = "aws iam get-account-summary --output=json"
        try:
            policy_str = subprocess.check_output(cmd, shell=True)
            self.mfapolicy = json.loads(policy_str.decode('utf-8'))
        except:
            self.fail("Unable to get account summary, please run it again")


# 1.13 Ensure MFA is enabled for the "root" account
class RootMFAEnabledPolicyTest(AccountMfaPolicyTest):

    def runTest(self):
        section = "1.13"
        sectionstatement = "Ensure MFA is enabled for the root account"
        assertEqualHandler(self, self.mfapolicy['SummaryMap']['AccountMFAEnabled'], 1, section, sectionstatement,
                           "MFA is not enabled for root account")  # 1 means mfa enabled

# 1.14 Ensure hardware MFA is enabled for the "root" account


class RootHardwareMFAEnabledPolicyTest(AccountMfaPolicyTest):

    def runTest(self):
        section = "1.14"
        sectionstatement = "Ensure hardware MFA is enabled for the root account"
        assertEqualHandler(self, self.mfapolicy['SummaryMap']['AccountMFAEnabled'], 1, section, sectionstatement,
                           "MFA is not enabled for root account")  # 1 means mfa enabled

        try:
            mfaDevices = subprocess.check_output("aws iam list-virtual-mfa-devices", shell=True)
            self.mfaDevices = json.loads(mfaDevices.decode('utf-8'))
            mfaDevicesList = self.mfaDevices['VirtualMFADevices']
        except:
            self.fail("Unable to fetch the list of virtual mfa devices")

        if len(mfaDevicesList) > 0:
            try:
                accountnumber = subprocess.check_output('aws sts get-caller-identity --output json --query "Account"', shell=True)
                self.accountnumber = json.loads(accountnumber.decode('utf-8'))
            except:
                self.fail("Unable to fetch the account number of your aws account")
            for eachVirtualDevice in mfaDevicesList:
                assertEqualHandler(eachVirtualDevice['SerialNumber'], "arn:aws:iam::_" + self.accountnumber +
                                   "_:mfa/root-account-mfadevice", "hardware MFA is enabled for the root account")
        else:
            self.fail("MFA is not enabled for root account")

# 1.16 Ensure IAM policies are attached only to groups or roles


class IAMPolicyGroupsRolesTest(unittest.TestCase):

    def setUp(self):
        cmd = "aws iam list-users --output=json"
        try:
            users_list = subprocess.check_output(cmd, shell=True)
            self.users_list = json.loads(users_list.decode('utf-8'))
        except:
            self.fail("Unable to get the list of IAM users, please run it again")

    def runTest(self):
        section = "1.16"
        sectionstatement = "Ensure IAM policies are attached only to groups or roles"
        for user_obj in self.users_list['Users']:
            userName = user_obj['UserName']
            try:
                attached_policies_list = subprocess.check_output(
                    "aws iam list-attached-user-policies --user-name " + userName, shell=True)
                attached_policies_list = json.loads(
                    attached_policies_list.decode('utf-8'))
            except:
                asertFailHandler(self, section, sectionstatement,
                                 "Unable to get list of attached user iam policies, please run it again")
            assertEqualHandler(self, len(attached_policies_list[
                'AttachedPolicies']), 0, section, sectionstatement, "IAM Policies are attached to user " + userName)
            try:
                policies_list = subprocess.check_output(
                    "aws iam list-user-policies --user-name " + userName, shell=True)
                policies_list = json.loads(policies_list.decode('utf-8'))
            except:
                asertFailHandler(self, section, sectionstatement, "Unable to get list of user policies, please run it again")
            assertEqualHandler(self, self, len(policies_list[
                'PolicyNames']), 0, section, sectionstatement, "IAM Policies should be attached to only groups and roles but here it is attached with user " + user_obj['UserName'])


class GetPolicyList(unittest.TestCase):

    def setUp(self):
        cmd = "aws iam list-policies --output=json"
        try:
            policyList = subprocess.check_output(cmd, shell=True)
            self.policyList = json.loads(policyList.decode('utf-8'))
        except:
            self.fail("Unable to get the list of policies, please run it again")


# 1.20 Ensure a support role has been created to manage incidents with AWS Support
class SupportRoleManageIncidentsTest(GetPolicyList):

    def runTest(self):
        section = "1.20"
        sectionstatement = "Ensure a support role has been created to manage incidents with AWS Support"
        for policyObj in self.policyList['Policies']:
            if policyObj['PolicyName'] == 'AWSSupportAccess':
                try:
                    policyAttached = subprocess.check_output(
                        "aws iam list-entities-for-policy --policy-arn " + policyObj['Arn'], shell=True)
                    policyAttached = json.loads(policyAttached.decode('utf-8'))
                except:
                    asertFailHandler(self, section, sectionstatement, "Unable to get policy entity list, please run it again")
                assertNotEqualHandler(self, len(policyAttached[
                    'PolicyRoles']), 0, section, sectionstatement, "No IAM role is there to manage incidents with AWS support")
                assertNotEqualHandler(self, len(policyAttached[
                    'PolicyGroups']), 0, section, sectionstatement, "No group is there to manage incidents with AWS support")
                assertNotEqualHandler(self, len(policyAttached[
                    'PolicyUsers']), 0, section, sectionstatement, "No user is there to manage incidents with AWS support")

# 1.21 Do not setup access keys during initial user setup for all IAM users that have a console password


class InitialAccessKeysSetUpPolicy(RootAccountTest):

    def runTest(self):
        section = "1.21"
        sectionstatement = "Do not setup access keys during initial user setup for all IAM users that have a console password"
        for user in self.user_data:
            lastUsedRootAccessKey1Date = user.split(",")[globalVariablesEnum.accesskey1LastUsed.value]
            accessKey1Active = user.split(",")[globalVariablesEnum.accessKey1Active.value]
            accessKey2Active = user.split(",")[globalVariablesEnum.accessKey2Active.value]
            lastUsedRootAccessKey2Date = user.split(",")[globalVariablesEnum.accesskey2LastUsed.value]
            userName = user.split(",")[0]
            if accessKey1Active == 'true':
                assertEqualHandler(self, lastUsedRootAccessKey1Date, 'N/A', section, sectionstatement,
                                   "Unused access_key_1 should be deleted for " + str(userName))
            if accessKey2Active == 'true':
                assertEqualHandler(self, lastUsedRootAccessKey2Date, 'N/A', section, sectionstatement,
                                   "Unused access_key_2 should be deleted for " + str(userName))

# 1.22 Ensure IAM policies that allow full "*:*" administrative privileges are not created Slow

# time consuming need to check


# class PolicyPrivilegesTest(GetPolicyList):

#     def runTest(self):
#         section = "1.22"
#         sectionstatement = "Ensure IAM policies that allow full *:* administrative privileges are not created"

#         for policyObj in self.policyList['Policies']:
#             try:
#                 print("checking policy  --policy-arn " + policyObj['Arn'])
#                 policyVersion = subprocess.check_output("aws iam get-policy-version  --policy-arn " +
#                                                         policyObj['Arn'] + " --version-id " + policyObj['DefaultVersionId'], shell=True)
#                 policyVersion = json.loads(policyVersion.decode('utf-8'))
#             except:
#                 asertFailHandler(self, section, sectionstatement, "Unable to get policy version for " +
#                                  policyObj['Arn'] + ", please run it again")
#             if policyVersion:
#                 statementsList = policyVersion[
#                     'PolicyVersion']['Document']['Statement']
#                 for policyStatement in statementsList:
#                     if CheckArrayOrString(policyStatement, 'Resource') == "*":
#                         if CheckArrayOrString(policyStatement, 'Action') == "*":
#                             assertNotEqualHandler(self, policyStatement[
#                                 'Effect'], "Allow", section, sectionstatement, "Full administrative privileges should not be given  to policy " + policyObj['PolicyName'])


def CheckArrayOrString(policyStatement, key):
    if type(policyStatement) == dict:
        try:
            if (len(policyStatement[key]) > 0):
                return policyStatement[key][0]
            else:
                return policyStatement[key]
        except:
            return None


# Created function to convert string date into date obj
def calNoOfDays(today_date, credUsedDate):
    # No information case or M/A case
    try:
        today_date = datetime.datetime.now()
        dateObj = datetime.datetime.strptime(credUsedDate, '%Y-%m-%dT%H:%M:%S+00:00')
        return (today_date - dateObj).days
    except:
        return 0


def assertEqualHandler(self, arg1, arg2, section, sectionstatement, comment):
    self.assertEqual(arg1, arg2, formatMessage(section, sectionstatement, comment))


def assertNotEqualHandler(self, arg1, arg2, section, sectionstatement, comment):
    self.assertNotEqual(arg1, arg2, formatMessage(section, sectionstatement, comment))


def assertLessEqualHandler(self, arg1, arg2, section, sectionstatement, comment):
    self.assertLessEqual(arg1, arg2, formatMessage(section, sectionstatement, comment))


def asertFailHandler(self, section, sectionstatement, comment):
    self.fail(formatMessage(section, sectionstatement, comment))


def formatMessage(section, sectionstatement, comment):
    msg = "IASTFERR:%s:%s:%s" % (section, sectionstatement, comment)
    return msg

# if __name__ == '__main__':
#     unittest.main()
