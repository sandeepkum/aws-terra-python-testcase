# Basic AWS Architecture Test Framework

This provides a test framework for testing Cloud Infrastructure. As an example, a simple two-tier architecture on Amazon
Web services is tested. The framework tests against the rules specified in AWS CIS Benchmarks. See <https://www.cisecurity.org/cis-benchmarks/>. The [terraform](https://www.terraform.io/) script that creates this test infrastructure is adapted from https://github.com/terraform-providers/terraform-provider-aws.

# Technologies used

- Python 
- Terraform
- AWS

# Setup
To setup the infrastructure run the following command:

```
terraform apply 
```

Before running the testscript, make sure you have the dependencies. So run:

```
pip3 install -r requirements.txt
```

To run the test cases of Identity and Access Management section, run the following:

```
python -m unittest -v test_identity_access_mgmt_cases

```

To run a specific test only, run the following:

```
python -m unittest -v test_identity_access_mgmt_cases.IAMPolicyTest
```
To run the test cases of Networking section, run the following:

```
python -m unittest -v test_networking_cases
```

To run a specific test only, run the following:

```
python -m unittest -v test_networking_cases.Ingress22test
```
To run the test cases of Logging section, run the following:

```
python -m unittest -v test_logging_cases
```

To run a specific test only, run the following:

```
python -m unittest -v test_logging_cases.CloudTrailEnabledTest
```
To run the test cases of Monitoring section, run the following:

```
python -m unittest -v test_monitoring_cases
```

To run a specific test only, run the following:

```
python -m unittest -v test_monitoring_cases.MetricFilterAlarmEnableUnAuthoriZedCallTest
```

To run the all the test case within a directory, run the following:

```
python -m unittest discover -vv

```
