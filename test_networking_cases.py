#!/usr/bin/env python2

""" Followed CIS_Amazon_Web_Services_Foundations_Benchmark_v1.2.0 """

import unittest
import lucid
import subprocess
import json
import base64

#import pdb; pdb.set_trace()
regions = [
    "us-east-2",
    "us-east-1",
    "us-west-1",
    "us-west-2",
    "ap-south-1",
    "ap-northeast-2",
    "ap-southeast-1",
    "ap-southeast-2",
    "ap-northeast-1",
    "ca-central-1",
    "eu-central-1",
    "eu-west-1",
    "eu-west-2",
    "eu-west-3",
    "sa-east-1",
]


class InfraBaseTestCase(unittest.TestCase):

    def setUp(self):
        self.infra = lucid.getInfraInfo('default', 'ap-south-1')

# common function to test port inbound rules


def PortPolicyTest(port, self, section, sectionstatement):
    for eachRegion in regions:
        infra = lucid.getSecurityGroupsInfo('default', str(eachRegion))
        security_grps = infra['securitygroup']['SecurityGroups']
        for securityGrp in security_grps:
            cidrlist = [security_grp['IpRanges'] for security_grp in securityGrp['IpPermissions']
                        if 'FromPort' in security_grp and security_grp['FromPort'] == port]
            if len(cidrlist) > 0:
                for count in range(len(cidrlist)):
                    assertNotEqualHandler(self, cidrlist[0][count]['CidrIp'], "0.0.0.0/0", section, sectionstatement, "GroupName: " + str(securityGrp['GroupName']) + " GroupId: " + str(
                        securityGrp['GroupId']) + " security groups should not allow ingress from 0.0.0.0/0 to port " + str(port) + " for region: " + str(eachRegion))

# 4.1 Ensure no security groups allow ingress from 0.0.0.0/0 to port 22

# check for 22 inside port range is missing to DO


class Ingress22test(unittest.TestCase):

    def runTest(self):
        section = "4.1"
        sectionstatement = "Ensure no security groups allow ingress from 0.0.0.0/0 to port 22"
        PortPolicyTest(22, self, section, sectionstatement)

# 4.2 Ensure no security groups allow ingress from 0.0.0.0/0 to port 3389

# TODO: check for 3389 inside port range is missing


class Ingress3389test(unittest.TestCase):

    def runTest(self):
        section = "4.2"
        sectionstatement = "Ensure no security groups allow ingress from 0.0.0.0/0 to port 3389"
        PortPolicyTest(3389, self, section, sectionstatement)

# 4.3 Ensure the default security group of every VPC restricts all traffic

# NOT SURE


class vpcRestrictsTraffic(unittest.TestCase):

    def runTest(self):
        section = "4.3"
        sectionstatement = "Ensure the default security group of every VPC restricts all traffic"
        for eachRegion in regions:
            infra = lucid.getSecurityGroupsInfo('default', str(eachRegion))
            security_grps = infra['securitygroup']['SecurityGroups']
            for securityGrp in security_grps:
                assertEqualHandler(self, len(securityGrp['IpPermissions']), 0, section, sectionstatement, "GroupName: " + str(securityGrp['GroupName']) + " GroupId: " + str(securityGrp['GroupId']) +
                                   " security group of VPC should restrict all traffic" + " for region: " + str(eachRegion))


def assertEqualHandler(self, arg1, arg2, section, sectionstatement, comment):
    self.assertEqual(arg1, arg2, formatMessage(section, sectionstatement, comment))


def assertNotEqualHandler(self, arg1, arg2, section, sectionstatement, comment):
    self.assertNotEqual(arg1, arg2, formatMessage(section, sectionstatement, comment))


def formatMessage(section, sectionstatement, comment):
    msg = "IASTFERR:%s:%s:%s" % (section, sectionstatement, comment)
    return msg
